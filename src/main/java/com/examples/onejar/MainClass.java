package com.examples.onejar;


import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;

import org.springframework.web.filter.DelegatingFilterProxy;

import com.sun.grizzly.http.embed.GrizzlyWebServer;
import com.sun.grizzly.http.servlet.ServletAdapter;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;

public class MainClass {
	public static final File STOP_FILE = new File(".stop");
	public static void main(String[] args) throws IOException, InterruptedException {
		if (args.length<3){
			System.out.println("enviroment parameter need to be set. usage : |enviroment|portNumber|hostName");
			System.exit(0);
		}
		STOP_FILE.delete();
		String enviroment = args[0];
		int port = Integer.parseInt(args[1]);
		String hostName = args[2];
		System.out.println("Starting "+enviroment+" enviroment" + "at port "+port+" for "+hostName);
		long start = System.currentTimeMillis();
		ServletAdapter jAdapter = new ServletAdapter();
		jAdapter.setContextPath("/");

		// For jersey + Spring
		jAdapter.setServletInstance(new SpringServlet());
		System.out.println("Classpath: \n "+System.getProperty("java.class.path"));
		jAdapter.addContextParameter("contextConfigLocation", "classpath:spring-context.xml,spring-context_dev.xml,spring-context_prod.xml");
		
		jAdapter.addServletListener("org.springframework.web.context.ContextLoaderListener");
		jAdapter.addServletListener("org.springframework.web.context.request.RequestContextListener");
		jAdapter.addContextParameter("spring.profiles.active", enviroment);
		

		// For Spring Security
		jAdapter.addFilter(new DelegatingFilterProxy(),
				"springSecurityFilterChain", null);

		GrizzlyWebServer grizzlyServer = new GrizzlyWebServer(hostName, port, "",
				false);
		grizzlyServer.addGrizzlyAdapter(jAdapter, new String[] { "/" });
		grizzlyServer.start();
		String pid = ManagementFactory.getRuntimeMXBean().getName();
		pid = pid.substring(0, pid.indexOf('@'));
		System.out.println(enviroment+" enviroment" + " at port "+port+" for "+hostName+" started successfully in "+(System.currentTimeMillis()-start)+"ms with pid :"+pid);
		File file = new File(".stop"); 
		while(!file.exists()){
			Thread.sleep(2000);
		}
		grizzlyServer.stop();
		file.delete();
		System.out.println("System shutdown complete");
	}
}
