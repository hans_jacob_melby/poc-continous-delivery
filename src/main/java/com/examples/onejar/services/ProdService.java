package com.examples.onejar.services;

import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.examples.onejar.resources.Service;

@Component
@Profile("prod")
@Primary
public class ProdService implements Service {

	public String getResponse() {
		return "Hello from production";
	}

}
