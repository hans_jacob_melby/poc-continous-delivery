package com.examples.onejar.services.stubs;


import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.examples.onejar.resources.Service;

@Component
@Profile("dev")
public class TestService implements Service {

	
	public String getResponse() {
		return "Hello from stub";
	}

}
