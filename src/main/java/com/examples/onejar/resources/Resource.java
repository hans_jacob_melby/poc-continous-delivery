package com.examples.onejar.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


import org.springframework.stereotype.Component;

@Component
@Path("/")
public class Resource {
	Service service;
	
	public void setService(Service service) {
		this.service = service;
	}
	@GET 
    @Produces("text/html")
	public String getIt()   {												
		return service.getResponse();
    }	
}