package com.examples.onejar.services;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.examples.onejar.resources.Service;
public class ProdServiceTest {

	
	private static final String EXPECTED_RESPONSE = "Hello from production";
	@Test(groups="SLOW_TEST")
	    public void testCorrectResponse() throws Exception {
	           Service service = new ProdService();
	           Assert.assertEquals(service.getResponse(), EXPECTED_RESPONSE);
	           
	    }
	
	
}
