package com.examples.onejar.services.stubs;

import org.testng.Assert;

import org.testng.annotations.Test;

public class TestTestService {

	private static final String EXPECTED_RESPONSE = "Hello from stub";
	@Test(groups="UNIT_TEST")
	    public void testCorrectResponse() throws Exception {
	           TestService service = new TestService();
	           Assert.assertEquals(service.getResponse(), EXPECTED_RESPONSE);
	           
	    }
	

}
